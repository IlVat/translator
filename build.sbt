name := "translator"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.8",
  "com.typesafe.akka" %% "akka-http-testkit" % "2.4.8",
  "com.typesafe.akka" %% "akka-actor" % "2.4.8",
  "com.typesafe.slick" % "slick_2.11" % "3.1.1",
  "org.slf4j" % "slf4j-log4j12" % "1.7.21",
  "org.scalatest" % "scalatest_2.11" % "2.2.6"
)

Revolver.settings
