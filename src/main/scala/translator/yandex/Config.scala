package translator.yandex

import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory

object Config {
  private val configFactory = ConfigFactory.load().getConfig("app")
  //App configs
  val serviceName = configFactory.getString("serviceName")
  val timeout = configFactory.getInt("timeout")
  val port = configFactory.getInt("port")
  val host = configFactory.getString("host")
  val actorSystemName = configFactory.getString("actorSystemName")

  object YandexTranslatorConf {
    private val config = configFactory.getConfig("yandex-translator")

  }



  object MongoDB {
    private val mongoConfig = configFactory.getConfig("mongo")

  }
}
